import { Injectable, inject } from '@angular/core';
import { HttpService } from './http.service';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    http = inject(HttpService);
    router = inject(Router);

    login({ email, password }: { email: string; password: string }) {
        this.http
            .post<any>({
                link: '',
                opj: { email: email, password: password },
            })
            .subscribe((val: any) => {
                localStorage.setItem('token', val.token);
                localStorage.setItem(
                    'permissions',
                    JSON.stringify(val.user.permissions)
                );
                this.router.navigate(['']);
            });
    }
    logout() {
        // logout
        localStorage.clear();
        this.router.navigate(['login']);
    }
}
