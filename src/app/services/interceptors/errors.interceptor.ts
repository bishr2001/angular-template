import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {
    constructor(private message: MessageService) {}

    faild(message: string) {
        this.message.add({
            severity: 'error',
            summary: 'Error',
            detail: message,
        });
    }

    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                let errorMsg = '';
                if (error.error instanceof ErrorEvent) {
                    // this.faild('user error');
                    // user
                } else {
                    // this.faild('server error');
                    // server
                    switch (error.status) {
                        case 401: //login
                            break;
                    }
                }
                return throwError(errorMsg);
            })
        );
    }
}
