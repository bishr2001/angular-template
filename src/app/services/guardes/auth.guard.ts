import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const authGuard: CanActivateFn = (route, state) => {
    const router = inject(Router);

    const val = localStorage.getItem('token') ? true : false;

    if (!val) {
        router.navigate(['/login']);
    }

    return val;
};
