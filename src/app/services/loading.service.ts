import { Injectable, inject, signal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NgxSpinnerService } from 'ngx-spinner';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class LoadingService {
    $loading = new BehaviorSubject<number>(0);

    private state = signal<number>(0);

    spinner = inject(NgxSpinnerService);

    constructor() {
        this.$loading.pipe(takeUntilDestroyed()).subscribe((val) => {
            this.state.update(() => {
                return val;
            });
            if (val == 0) {
                this.spinner.hide();
            } else {
                this.spinner.show();
            }
        });
    }

    increse() {
        this.$loading.next(this.state() + 1);
    }
    decrice() {
        this.$loading.next(this.state() > 0 ? this.state() - 1 : 0);
    }
}
