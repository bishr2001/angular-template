import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { tap } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { LoadingService } from './loading.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Observable } from 'rxjs';

export class requestOptions {
  link: string | undefined;
  successToast?: boolean = true;
  failedToast?: boolean = true;
  successMessage?: string = 'request succeeded';
  errorMessage?: string = 'request faild';
  showloader?: boolean = true;
  opj?: any = undefined;
}

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  server: string = '';

  http = inject(HttpClient);
  message = inject(MessageService);
  loading = inject(LoadingService);

  attatchPipe<T>(stream: Observable<T>, op: requestOptions) {
    if (op.showloader) this.loading.increse();
    return stream.pipe(
      takeUntilDestroyed(),
      tap(
        () => {
          this.showSuccess(!!op.successToast, op.successMessage + '');
          if (op.showloader) this.loading.decrice();
        },
        () => {
          this.showError(!!op.failedToast, op.errorMessage + '');
          if (op.showloader) this.loading.decrice();
        }
      )
    );
  }

  get<T>(op: requestOptions) {
    const request = this.http.get<T>(this.server + 'api/' + op.link);
    return this.attatchPipe(request, op);
  }
  post<T>(op: requestOptions) {
    const request = this.http.post<T>(this.server + 'api/' + op.link, op.opj);
    return this.attatchPipe(request, op);
  }

  delete<T>(op: requestOptions) {
    const request = this.http.delete<T>(this.server + 'api/' + op.link);
    return this.attatchPipe(request, op);
  }

  put<T>(op: requestOptions) {
    const request = this.http.put<T>(this.server + 'api/' + op.link, op.opj);
    return this.attatchPipe(request, op);
  }
  patch<T>(op: requestOptions) {
    const request = this.http.patch<T>(this.server + 'api/' + op.link, op.opj);
    return this.attatchPipe(request, op);
  }

  showSuccess(show: boolean, massage: string) {
    if (show)
      this.message.add({
        severity: 'success',
        summary: 'Success',
        detail: massage,
      });
  }
  showError(show: boolean, massage: string) {
    if (show) {
      this.message.add({
        severity: 'error',
        summary: 'Error',
        detail: massage,
      });
    }
  }
}
